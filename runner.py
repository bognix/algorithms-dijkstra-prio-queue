from copy import copy
from lib.graph import Graph
from lib.prio_queue import PrioQueue

vertices = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
edges = [(1, 2, 7), (2, 4, 15), (2, 3, 20), (3, 4, 1), (1, 5, 15), (5, 4, 10), (5, 10, 8), (10, 6, 15), (6, 7, 3), (4, 7, 3), (8, 7, 4), (3, 8, 2), (5, 10, 8), (9, 1, 1), (5, 9, 5)]

def intersection(list1, list2):
    result = copy(list2)
    for element in list2:
        if element in list1:
            result.remove(element)
    return result


def dijkstry():
    graph = Graph(v=vertices, e=edges)
    s = []
    graph.initialize_single_source(0)
    graph_vertices = copy(graph.vertices)
    prio_queue = PrioQueue(vertices, c=graph_vertices)
    while prio_queue.complex_elements_count > 0:
        min_vertex = prio_queue.extract_minimum_complex()
        s.append(min_vertex)
        if len(graph.get_children(min_vertex)) > 0:
            for vertex in graph.get_children(min_vertex):
                graph.relax(min_vertex.id, vertex.id)
        remaining_vertices = intersection(s, graph.vertices)
        prio_queue = PrioQueue(vertices, c=remaining_vertices)
    for vertex in s:
        print "To vertex: %s, shortest path: %s" % (vertex.id, vertex.d)



if __name__ == '__main__':
    dijkstry()