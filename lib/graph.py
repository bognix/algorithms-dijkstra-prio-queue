infinity = 100000


class Graph():
    def __init__(self, *args, **kwargs):
        self.vertices = []
        self.edges = []
        if kwargs.has_key('v'):
            for i in range(len(kwargs['v'])):
                self.vertices.append(Vertex(i+1))
        if kwargs.has_key('e'):
            edges_desc = kwargs['e']
            for i in range(len(edges_desc)):
                self.edges.append(Edge((Vertex(edges_desc[i][0]), Vertex(edges_desc[i][1]), edges_desc[i][2])))

    def initialize_single_source(self, source_index):
        source_vertex = self.vertices[source_index]
        for vertex in self.vertices:
            vertex.d = infinity
            vertex.pi = None
        source_vertex.d = 0

    def get_children(self, vertex):
        children = []
        for edge in self.edges:
            if edge.from_vertex.id == vertex.id:
                children.append(edge.to_vertex)
        return children

    def get_weight(self, vertex_start, vertex_end):
        # if vertices are connected
        for edge in self.edges:
            if edge.from_vertex.id == vertex_start.id:
                if edge.to_vertex.id == vertex_end.id:
                    return edge.weight

    def set_vertex_pi(self, id, value):
        for i in range(len(self.vertices)):
            if self.vertices[i].id == id:
                self.vertices[i].pi = value

    def set_vertex_d(self, id, value):
        for i in range(len(self.vertices)):
            if self.vertices[i].id == id:
                self.vertices[i].d = value

    def get_vertex_by_id(self, id):
        for vertex in self.vertices:
            if vertex.id is id:
                return vertex

    def relax(self, vertex_start_id, vertex_end_id):
        vertex_end = self.get_vertex_by_id(vertex_end_id)
        vertex_start = self.get_vertex_by_id(vertex_start_id)
        if vertex_end.d > vertex_start.d + self.get_weight(vertex_start, vertex_end):
            self.set_vertex_d(vertex_end_id, vertex_start.d + self.get_weight(vertex_start, vertex_end))
            self.set_vertex_pi(vertex_end_id, vertex_start_id)

class Vertex():
    def __init__(self, id):
        self.id = id
        #d value
        self.d = infinity
        #pi value
        self.pi = None


class Edge():
    def __init__(self, edge_desc):
        self.from_vertex = edge_desc[0]
        self.to_vertex = edge_desc[1]
        self.weight = edge_desc[2]


__author__ = 'bogna'
