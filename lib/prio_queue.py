from lib.binary_heap import BinaryHeap


class PrioQueue():

    elements = []

    def __init__(self, elements, *args, **kwargs):
        self.elements = elements
        if kwargs.has_key('c'):
            self.complex_elements = kwargs['c']
            self.complex_elements_count = len(self.complex_elements)

    def insert(self, element):
        self.elements.append(element)

    def minimum(self):
        return min(self.elements)

    def extract_minimum(self):
        binary_heap = BinaryHeap(elements=self.elements)
        return binary_heap.extract_min()

    def extract_minimum_complex(self):
        binary_heap = BinaryHeap()
        binary_heap.build_heap_complex(self.complex_elements)
        min = binary_heap.extract_min_complex()
        self.complex_elements_count = binary_heap.heap_size
        return min

    def decrease_key(self, element, key):
        if self.elements.index(element) < key:
            raise Exception("Key smaller then provided")
        else:
            old_key = self.elements.index(element)
            current_value = self.elements[key]
            self.elements[key] = element
            self.elements[old_key] = current_value

__author__ = 'bogna'
