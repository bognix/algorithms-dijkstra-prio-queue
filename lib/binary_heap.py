def swap(elements_list, index1, index2):
    element1 = elements_list[index1]
    element2 = elements_list[index2]
    elements_list[index1] = element2
    elements_list[index2] = element1
    return elements_list


class BinaryHeap():
    def __init__(self, *args, **kwargs):
        self.heap_size = 0
        self.elements = []
        self.complex_elements = []
        if kwargs.has_key('elements'):
            self.build_heap(kwargs['elements'])

    def parent(self, index):
        if index > 0:
            index -= 1
        return int(index / 2)

    def left(self, index):
        # +1 because we are indexing from 0 not from 1
        return 2 * index + 1

    def right(self, index):
        # +2 because we are indexing from 0 not from 1
        return 2 * index + 2

    def min_heapify(self, index):
        left_index = self.left(index)
        right_index = self.right(index)
        if left_index < self.heap_size and self.elements[left_index] < self.elements[index]:
            smallest_index = left_index
        else:
            smallest_index = index
        if right_index < self.heap_size and self.elements[right_index] < self.elements[smallest_index]:
            smallest_index = right_index
        if smallest_index != index:
            self.elements = swap(self.elements, index, smallest_index)
            self.min_heapify(smallest_index)

    def min_heapify_complex(self, index):
        left_index = self.left(index)
        right_index = self.right(index)
        if left_index < self.heap_size and self.complex_elements[left_index].d < self.complex_elements[index].d:
            smallest_index = left_index
        else:
            smallest_index = index
        if right_index < self.heap_size and self.complex_elements[right_index].d < self.complex_elements[smallest_index].d:
            smallest_index = right_index
        if smallest_index != index:
            self.complex_elements = swap(self.complex_elements, index, smallest_index)
            self.min_heapify_complex(smallest_index)


    def build_heap(self, list_elements):
        self.heap_size = len(list_elements)
        self.elements = list_elements
        for i in reversed(range(int(len(list_elements) / 2))):
            self.min_heapify(i)

    def build_heap_complex(self, list_elements):
        self.heap_size = len(list_elements)
        self.complex_elements = list_elements
        for i in reversed(range(int(len(list_elements) / 2))):
            self.min_heapify_complex(i)

    def extract_min(self):
        if self.heap_size < 1:
            raise Exception('Heap is empty')
        min_element = self.elements[0]
        #assign first element value to the last element value
        self.elements[0] = self.elements[-1]
        #remove last element
        self.elements.pop(-1)
        self.heap_size = len(self.elements)
        self.min_heapify(0)
        return min_element

    def extract_min_complex(self):
        if self.heap_size < 1:
            raise Exception('Heap is empty')
        min_element = self.complex_elements[0]
        #assign first element value to the last element value
        self.complex_elements[0] = self.complex_elements[-1]
        #remove last element
        self.complex_elements.pop(-1)
        self.heap_size = len(self.complex_elements)
        self.min_heapify_complex(0)
        return min_element



__author__ = 'bogna'
