import unittest
from unittest_data_provider import data_provider
from lib.graph import Edge, Graph, Vertex

infinity = 100000


class GraphTestCases(unittest.TestCase):
    def test_initialize_single_source(self):
        graph = Graph(v=[1, 2, 3, 4, 5], e=[(1, 2, 7), (2, 4, 15), (2, 3, 20), (3, 4, 1), (1, 5, 15), (5, 4, 10)])
        graph.initialize_single_source(0)
        self.assertEqual(graph.vertices[0].d, 0)
        self.assertEqual(graph.vertices[1].d, infinity)


class EdgeTestCases(unittest.TestCase):
    edges = lambda: (
        ((Vertex(1), Vertex(1), Vertex(1)),),
        ((3, 6, 10),),
        ((3, 6, 10),)
    )

    @data_provider(edges)
    def test_create_edge(self, edge_desc):
        edge = Edge(edge_desc)
        self.assertEqual(edge.from_vertex, edge_desc[0])
        self.assertEqual(edge.to_vertex, edge_desc[1])
        self.assertEqual(edge.weight, edge_desc[2])


__author__ = 'bogna'
