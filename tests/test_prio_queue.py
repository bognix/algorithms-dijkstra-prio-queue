import unittest
from lib.prio_queue import PrioQueue


class PrioQueueTestCases(unittest.TestCase):

    def test_insert_without_duplication(self):
        expected_elements = [1, 2, 3, 4, 5]
        prio_queue = PrioQueue([1, 2, 3, 4])
        prio_queue.insert(5)
        self.assertListEqual(prio_queue.elements, expected_elements)

    def test_insert_with_duplication(self):
        expected_elements = [1, 2, 1]
        prio_queue = PrioQueue([1, 2])
        prio_queue.insert(1)
        self.assertListEqual(prio_queue.elements, expected_elements)

    def test_get_min(self):
        prio_queue = PrioQueue([1, 5, 6, 0, 7, 10, 2])
        self.assertEqual(prio_queue.minimum(), 0)

    def test_get_min_duplicate(self):
        prio_queue = PrioQueue([1, 5, 6, 1, 6, 7, 10, 2])
        self.assertEqual(prio_queue.minimum(), 1)

    def test_extract_min(self):
        prio_queue = PrioQueue([2, 3, 0, 1, 2])
        self.assertEqual(prio_queue.extract_minimum(), 0)

    def test_extract_min_duplicated(self):
        prio_queue = PrioQueue([2, 3, 1, 2, 1])
        self.assertEqual(prio_queue.extract_minimum(), 1)

    def test_decrease_key(self):
        prio_queue = PrioQueue([1,2,3,4])
        prio_queue.decrease_key(2, 0)
        expected_elements = [2,1,3,4]
        self.assertListEqual(prio_queue.elements, expected_elements)

    def test_decrease_key_duplicated_value(self):
        prio_queue = PrioQueue([1,2,2,4])
        prio_queue.decrease_key(2, 0)
        expected_elements = [2,1,2,4]
        self.assertListEqual(prio_queue.elements, expected_elements)

