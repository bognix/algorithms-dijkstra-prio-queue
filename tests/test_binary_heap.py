import unittest
from unittest_data_provider import data_provider
from lib.binary_heap import BinaryHeap, swap

class Vertex():
    def __init__(self, d):
        self.d = d

class BinaryHeapTestCases(unittest.TestCase):
    elements_for_heap = lambda: (
        ([3, 6, 10, 20, 0, 30, 11, 12, 10], [0, 3, 10, 10, 6, 30, 11, 12, 20]),
        ([0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 1]),
        ([100, 200, 101, 102, 99, 88], [88, 99, 100, 102, 200, 101]),
        ([100, 200, 101, 102, 99, -1], [-1, 99, 100, 102, 200, 101]),
    )

    elements_for_extract_min = lambda: (
        ([3, 6, 10, 20, 0, 30, 11, 12, 10], 0),
        ([0, 0, 0, 0, 0, 1], 0),
        ([100, 200, 101, 102, 99, 88], 88),
        ([100, 200, 101, 102, 99, -1], -1),
        ([2, 3, 0, 1, 2], 0),
        ([2, 3, 1, 1, 2], 1),
    )

    elements_for_extract_min_complex = lambda: (
        ([Vertex(7), Vertex(10), Vertex(11), Vertex(0)], 0),
        ([Vertex(0), Vertex(0), Vertex(0)], 0),
        ([Vertex(100), Vertex(200), Vertex(101), Vertex(102), Vertex(99), Vertex(88)], 88),
        ([Vertex(2), Vertex(3), Vertex(1), Vertex(1), Vertex(2)], 1),
    )

    @data_provider(elements_for_heap)
    def test_build_heap(self, elements, expected_result):
        binary_heap = BinaryHeap()
        binary_heap.build_heap(elements)
        self.assertSequenceEqual(expected_result, binary_heap.elements)

    @data_provider(elements_for_extract_min)
    def test_extract_min(self, elements, expected_result):
        binary_heap = BinaryHeap(elements=elements)
        self.assertEqual(binary_heap.extract_min(), expected_result)

    @data_provider(elements_for_extract_min_complex)
    def test_extract_min(self, elements, expected_result):
        binary_heap = BinaryHeap()
        binary_heap.build_heap_complex(elements)
        self.assertEqual(binary_heap.extract_min_complex().d, expected_result)

class SwapTestCases(unittest.TestCase):
    def test_swap(self):
        elements = [33, 70, 50, 102, 1500]
        expected_elements = [33, 50, 70, 102, 1500]
        result = swap(elements, 1, 2)
        self.assertListEqual(result, expected_elements)


__author__ = 'bogna'
